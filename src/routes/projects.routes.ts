import { Router } from 'express';

const projectsRouter = Router();

projectsRouter.get("/repositories", (request, response) => {
  return response.json({ success: "ok" });
});

projectsRouter.post("/repositories", (request, response) => {
  // TODO
});

projectsRouter.put("/repositories/:id", (request, response) => {
  // TODO
});

projectsRouter.delete("/repositories/:id", (request, response) => {
  // TODO
});

projectsRouter.post("/repositories/:id/like", (request, response) => {
  // TODO
});